// Bài 1: Tính lương nhân viên
/*
Input: 
 số ngày làm trong 1 tháng
 tiền lương 1 ngày 
 Step:
 s1: tạo biến chứa tiền lương 1 ngày & số ngày làm trong tháng
 s2: tạo biến chứa tổng tiền lương 1 tháng 
 Ouput: tiền lương 1 tháng 
 */
var luong1Ngay = 100.0;
var ngayLam = 26;
var tongLuong = null;
tongLuong1Thang = ngayLam * luong1Ngay;
console.log("tongLuong1Thang: ", tongLuong1Thang);

// Bài 2 - Tính tổng
/**
 * Input: nhập dô 5 số thực
 * Step:
 * s1: tạo biến chứa 5 số thực
 * s2: cộng 5 số lại với nhau
 * Output: tổng của 5 số thực
 */
var a = 5;
var b = 10;
var c = 15;
var d = 20;
var e = 25;
var tong = null;
tong = a + b + c + d + e;
console.log("tong: ", tong);

// Bài 3 - Quy đổi tiền
/**
 * Input: tiền USD
 * Step:
 * s1: tạo biến chứa tiền USD,  tiền VND
 * s2: tạo biến chứa tiền đã quy đỗi
 * Ouput: ra tiền VND
 */
var usd = 6;
var vnd = 23.5;
var soTien = null;
soTien = usd * vnd;
console.log("soTien: ", soTien);

// Bài 4 - Tính diện tích, chu vi hình chữ nhật
/*
 *Input: chiều dài, chiều
 * Step:
 * s1: tạo biến chứa chiều dài, chiều rộng
 * s2: tạo biến chứa diện tích, chu vi
 * Output: in ra diện tích, chu vi
 */
var chieuDai = 5;
var chieuRong = 5;
var chuVi = null;
var dienTich = null;
chuVi = (chieuDai + chieuRong) * 2;
console.log("chuVi: ", chuVi);
dienTich = chieuDai * chieuRong;
console.log("dienTich: ", dienTich);

// Bài 5 - Tính tổng 2 ký số
/**
 * Input: 1 số có chữ số
 * Step:
 * st1: tạo biến chứa số nhập vào
 * st2: tạo biến chứa số hàng đv, hàng chục
 * st3: cộng 2 số lại với nhau
 * Output: tổng của 2 chữ số nhập dô
 * */
var n = 12;
var result = null;
var so_hang_dv = n % 10;
var so_hang_chuc = n / 10;
result = so_hang_dv + so_hang_chuc;
result = Math.floor(result);
console.log("result: ", result);
